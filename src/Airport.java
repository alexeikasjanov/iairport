public class Airport {// Airport
    public static volatile Runway RUNWAY = new Runway();   //1 взлетная полоса
    public static void main(String[] args) throws InterruptedException {
        Plane plane1 = new Plane("Plane #1");
        Plane plane2 = new Plane("Plane #2");
        Plane plane3 = new Plane("Plane #3");
    }
    private static void waiting() {       // time of waiting
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
        }
    }
    public static void takingOff() {      // time of take off
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
        }
    }
    public static class Plane extends Thread {
        public Plane(String name) {
            super(name);
            start();
        }
        public void run() {
            boolean isAlreadyTakenOff = false;
            while (!isAlreadyTakenOff) {
                if (RUNWAY.trySetTakingOffPlane(this)) {    //if take off line is empty - take if
                    System.out.println(getName() + " take off");
                    takingOff();    // take off
                    System.out.println(getName() + " at the sky");
                    isAlreadyTakenOff = true;
                    RUNWAY.setTakingOffPlane(null);
                } else if (!this.equals(RUNWAY.getTakingOffPlane())) {  //if take off line is not empry
                    System.out.println(getName() + " waiting ");
                    waiting();     //waiting
                }
            }
        }
    }
    public static class Runway {      //Take off line
        private Thread t;

        public Thread getTakingOffPlane() {
            return t;
        }
        public void setTakingOffPlane(Thread t) {
            synchronized (this) {
                this.t = t;
            }
        }
        public boolean trySetTakingOffPlane(Thread t) {
            synchronized (this) {
                if (this.t == null) {
                    this.t = t;
                    return true;
                }
                return false;
            }
        }
    }
}
